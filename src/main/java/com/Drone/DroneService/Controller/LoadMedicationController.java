package com.Drone.DroneService.Controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.Drone.DroneService.Model.Drone;
import com.Drone.DroneService.Model.LoadMedication;
import com.Drone.DroneService.Model.Medication;
import com.Drone.DroneService.Service.DroneService;
import com.Drone.DroneService.Service.LoadMedicationService;
import com.Drone.DroneService.Service.MedicationService;

@RestController
@RequestMapping(value = "/LoadMedication")
public class LoadMedicationController {
	@Autowired
	private LoadMedicationService loadmedicationService;
	
	@Autowired
	private DroneService droneservice;
	
	@Autowired
	private MedicationService medicationService;
	
	@GetMapping("")
	public List<LoadMedication>  getLoadedMedications(){
		
		return loadmedicationService.getLoadedMedications();
		
	}
	
	@GetMapping("/{code}")
    public LoadMedication findByCode(@PathVariable String code) {
  	
        return loadmedicationService.findByCode(code);
        
    }
	
	@GetMapping("/drone/{serialno}")
    public LoadMedication findByDrone(@PathVariable String serialno) {
  	
        return loadmedicationService.findByDrone(serialno);
        
    }
	
	@PostMapping(path = "/{serialno}/{code}", consumes = "application/json")
    public String LoadMedication(@RequestBody LoadMedication loadMedication,@PathVariable String serialno,@PathVariable String code) {
		
	    Drone drone=droneservice.findBySerialNo(serialno);
	    
	    if(drone.getState()=="IDLE" || drone.getState()=="LOADING" & drone.getBattery()>25) {
	    	
	    	droneservice.updateState("LOADING",serialno);
	    	
		    Medication medication=medicationService.findByCode(code);
		    loadMedication.setDrone(drone);
		    loadMedication.setMedication(medication);
		    loadMedication.setCreatedon(LocalDate.now());
			
			loadmedicationService.Load(loadMedication);
			
			
    	
            return "succesfully loaded medications to a drone";
        
	    }else {
	    	return "No idle drones or no drones with enough battery";
	    }
        
    }
	

}
