package com.Drone.DroneService.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.Drone.DroneService.Model.Medication;
import com.Drone.DroneService.Service.MedicationService;

@RestController
@RequestMapping(value = "/medication")
public class MedicationController {
	@Autowired
	private MedicationService  medicationService;
	
	@GetMapping("/load")
	public String  loadMedications(){
		preloadData();
		return "data Loaded";
	}
	
	@GetMapping("")
	public List<Medication>  getMedications(){
		
		return medicationService.getMedications();
		
	}
	
	@GetMapping("/{code}")
    public Medication findByCode(@PathVariable String code) {
  	
        return medicationService.findByCode(code);
        
    }
	
	@PostMapping(path = "/add", consumes = "application/json")
    public String addMedication(@RequestBody Medication medication) {

		medicationService.saveMedication(medication);
    	
        return "succesfully Added a medication";
        
    }
	
	
	void preloadData() {
		
			Medication medication1 = new Medication("XSHD123","mulondo",400.0,"mulondo.png");
			Medication medication2 = new Medication("GFHDJ123","kigaji",150.0,"kigaji.jpg");
			Medication medication3 = new Medication("FDSJ12JD","asprin",200.0,"asprin.png");
			Medication medication4 = new Medication("WCBDN23","covidex",300.0,"covidex.png");
			Medication medication5 = new Medication("GKGHDHD12","panadol",100.0,"panadoal.jpg");
			
			medicationService.saveMedication(medication1);
			medicationService.saveMedication(medication2);
			medicationService.saveMedication(medication3);
			medicationService.saveMedication(medication4);
			medicationService.saveMedication(medication5);

	}

}
