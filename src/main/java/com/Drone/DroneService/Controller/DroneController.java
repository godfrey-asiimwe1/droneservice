package com.Drone.DroneService.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.Drone.DroneService.Model.Drone;
import com.Drone.DroneService.Service.DroneService;

@RestController
@RequestMapping(value = "/drone")
public class DroneController {
	
	@Autowired
	private DroneService droneService;
	
	@GetMapping("/load")
	public String  loadDrones(){
		preloadData();
		return "Data loaded";
	}
	
	@GetMapping("/idle")
	public List<Drone>  getIddleDrones(){
	
		return droneService.findIdleDrones();
	}
	
	@GetMapping("")
	public List<Drone>  getDrones(){
		return droneService.getDrones();	
	}
	
	@GetMapping("/{code}")
    public Drone findBySerialNo(@PathVariable String code) {
  	
        return droneService.findBySerialNo(code);
        
    }
	
	
	@PostMapping(path = "/register", consumes = "application/json")
    public String addDrone(@RequestBody Drone drone) {

    	droneService.saveDrone(drone);
    	
        return "succesfully Registered a drone";
        
    }
	
	void preloadData() {
		
		Drone drone1 = new Drone("SH23SJSJ","Middleweight",60.0,70.0,"IDLE");
		Drone drone2 = new Drone("WH23SDSK","Cruiserweight",70.0,60.0,"IDLE");
		Drone drone3 = new Drone("NH23SMSC","Heavyweight",90.0,80.0,"IDLE");
		Drone drone4 = new Drone("XH23SDS2","Middleweight",50.0,20.0,"IDLE");
		Drone drone5 = new Drone("QF23SKD3J","Lightweight",30.0,50.0,"IDLE");
		
		droneService.saveDrone(drone1);
		droneService.saveDrone(drone2);
		droneService.saveDrone(drone3);
		droneService.saveDrone(drone4);
		droneService.saveDrone(drone5);
		

}
	

}
