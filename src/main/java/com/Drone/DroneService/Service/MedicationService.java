package com.Drone.DroneService.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.Drone.DroneService.Model.Medication;
import com.Drone.DroneService.Repository.MedicationRepository;

@Service
@Transactional
public class MedicationService {
	@Autowired
	private MedicationRepository medicationRepository;
	
	public List<Medication> getMedications(){
		
		return medicationRepository.findAll();
		
	}
	
    public Medication saveMedication(Medication medication) {
		
		return medicationRepository.save(medication);
		
	}
    
    public Medication findByCode(String code) {
		return medicationRepository.findByCode(code);
    	
    }

}
