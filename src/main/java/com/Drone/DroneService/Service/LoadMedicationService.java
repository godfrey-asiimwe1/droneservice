package com.Drone.DroneService.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.Drone.DroneService.Model.LoadMedication;
import com.Drone.DroneService.Repository.LoadMedicationRepository;

@Service
@Transactional
public class LoadMedicationService {
	@Autowired
	private LoadMedicationRepository loadmedicationRepository;

	public List<LoadMedication> getLoadedMedications(){
		
		return loadmedicationRepository.findAll();
		
	}
	
    public LoadMedication Load(LoadMedication loadMedication) {
		
		return loadmedicationRepository.save(loadMedication);
		
	}
    
    public LoadMedication findByCode(String code) {
		return loadmedicationRepository.findByCode(code);
    	
    }
    
    public LoadMedication findByDrone(String serialno) {
		return loadmedicationRepository.findByDrone(serialno);
    	
    }
}
