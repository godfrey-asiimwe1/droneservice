package com.Drone.DroneService.Model;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "LoadMedication")
public class LoadMedication implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
	
	@Column(name = "source", columnDefinition = "VARCHAR(30) NOT NULL")
	private String source;
	
	@Column(name = "destination", columnDefinition = "VARCHAR(30) NOT NULL")
	private String destination;
	
	@Column(name = "createdon", columnDefinition = "VARCHAR(30) NOT NULL")
	private LocalDate createdon;
	
	@OneToOne(cascade = CascadeType.REMOVE)
	@JoinColumn(name = "fk_serial_no", referencedColumnName = "serial_no", unique = true)
	private Drone drone;

	@OneToOne(cascade = CascadeType.REMOVE)
	@JoinColumn(name = "fk_code", referencedColumnName = "code", unique = true)
	private Medication medication;
	
	public LoadMedication() {
		// TODO Auto-generated constructor stub
	}
	
	public LoadMedication(int id,String source, String destination,LocalDate createdon,
			Drone drone,Medication medication) {
		super();
		this.id=id;
		this.source=source;
		this.destination=destination;
		this.createdon=createdon;
		this.drone=drone;
		this.medication=medication;	
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public LocalDate getCreatedon() {
		return createdon;
	}

	public void setCreatedon(LocalDate createdon) {
		this.createdon = createdon;
	}

	public Drone getDrone() {
		return drone;
	}

	public void setDrone(Drone drone) {
		this.drone = drone;
	}

	public Medication getMedication() {
		return medication;
	}

	public void setMedication(Medication medication) {
		this.medication = medication;
	}
	
}
