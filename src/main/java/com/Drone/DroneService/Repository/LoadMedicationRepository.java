package com.Drone.DroneService.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.Drone.DroneService.Model.LoadMedication;

public interface LoadMedicationRepository extends JpaRepository<LoadMedication, Integer>{
	@Query(value = "SELECT * from LOAD_MEDICATION e where e.fk_serial_no =?1 ", nativeQuery = true) // using @query with
	LoadMedication findByDrone(@Param("serialno") String serialno);
	
	@Query(value = "SELECT * from LOAD_MEDICATION e where e.fk_code =?1", nativeQuery = true)
	LoadMedication findByCode(@Param("code") String code);
	
	@Query(value = "SELECT LOAD_MEDICATION.id,LOAD_MEDICATION.createdon, LOAD_MEDICATION.destination, LOAD_MEDICATION.fk_serial_no as drone, LOAD_MEDICATION.fk_code as medication, LOAD_MEDICATION.source from LOAD_MEDICATION ", nativeQuery = true)
	List<LoadMedication> findAll();

}
