package com.Drone.DroneService.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.Drone.DroneService.Model.Drone;

public interface DroneRepository extends JpaRepository<Drone, String> {
	
	@Query(value = "SELECT * from Drone ", nativeQuery = true)
	List<Drone> findAll();
	
	@Query(value = "SELECT * from Drone where Drone.serial_no =?1 ", nativeQuery = true)
	Drone findBySerialNo(@Param("code") String code);
	
	@Modifying
	@Query(value = "UPDATE Drone SET drone_state=?1 WHERE Drone.serial_no =?2 ", nativeQuery = true)
	int UpdateState(@Param("state") String state,@Param("serialNo") String serialNo);
	
	
	@Query(value = "SELECT * from Drone where Drone.drone_state ='IDLE' ", nativeQuery = true)
	List<Drone> findIdleDrones();

}
