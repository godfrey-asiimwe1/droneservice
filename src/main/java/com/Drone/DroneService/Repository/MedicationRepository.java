package com.Drone.DroneService.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.Drone.DroneService.Model.Medication;

public interface MedicationRepository extends JpaRepository<Medication, Integer>{
	
	@Query(value = "SELECT * from medication ", nativeQuery = true)
	List<Medication> findAll();
	
	@Query(value = "SELECT * from medication where medication.code =?1 ", nativeQuery = true)
	Medication findByCode(@Param("code") String code);

}
