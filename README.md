# DroneService

#to run this project--
at project root; run command: mvn spring-boot:run

#end points for Drone Object

using postman, you can test out different end points

using get --http://server:port/drone #this end point will list all avaliable drones. it will return empty list. 

To load sample data for drone.
using get --http://server:port/drone/load  #this will load sample drones for testing

To add/register a drone.
using post ---http://server:port/drone/register
Json
{
    "serialNumber": "UH23SJSJ",
    "model": "Middleweight",
    "weightLimit": 55,
    "battery": 40,
    "state": "IDLE"
}

To see a drone by serialNumber
using get-- http://server:port/drone/serialNumber(UH23SJSJ) # will return a json drone details
Json
{
    "serialNumber": "UH23SJSJ",
    "model": "Middleweight",
    "weightLimit": 55,
    "battery": 40,
    "state": "IDLE"
}

##get drones available for loading
To see a drone available for loading must be in iddle state
using get-- http://server:port/drone/idle # will return a json drone details


##Medications
using get --http://server:port/medication #this end point will list all avaliable medications. it will return empty list. 

To load sample data for medications.
using get --http://server:port/medication/load  #this will load sample medications for testing

To add a medication.
using post ---http://server:port/medication/add
Json body
{
	"code":"WQS12DweadQ",
	"name":"Asprin",
	"weight":60.0,
	"image":"asprin.jpg"
}

To see Medication by code
using get-- http://server:port/medication/code(WQS12DweadQ) # will return a json medication details
Json
{
	"code":"WQS12DweadQ",
	"name":"Asprin",
	"weight":60.0,
	"image":"asprin.jpg"
}

##Load Medication to the Drone
using post -- http://server:port/LoadMedication/Drone(SH23SJSJ)/medication(XSHD123)
Json body
{
	"source":"africa",
	"destination":"Europe"
}
 
To get loaded drones
using get -- http://server:port/LoadMedication  #will return a json loaded drones

##To check if the drone is loaded
using get -- http://server:port/LoadMedication/drone/serialno(SH23SJSJ)--sample
{
    "id": 11,
    "source": "africa",
    "destination": "Europe",
    "createdon": "2022-11-19",
    "drone": {
        "serialNumber": "SH23SJSJ",
        "model": "Middleweight",
        "weightLimit": 60,
        "battery": 20,
        "state": "IDLE"
    },
    "medication": {
        "code": "XSHD123",
        "name": "mulondo",
        "weight": 400,
        "image": "mulondo.png"
    }
}







